GCC = gcc
GCCFLAGS = -lpthread -lbladeRF -lvolk -lm

main: main.c
	$(GCC) $(GCCFLAGS) -o main main.c
	mkdir -p data

clean:
	rm main

