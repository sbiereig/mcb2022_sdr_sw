#!/usr/bin/python3
import argparse
import glob
import os
import numpy as np
import matplotlib.style
matplotlib.style.use('fast')
import scipy.signal as sps
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="Plot phase transients from file.")
parser.add_argument("filename", type=str, help="Name of the file to read from")
args = parser.parse_args()

fs = 20e6
lp_taps = sps.remez(128, [0, 1e6, 1e6+200e3, fs/2], [1, 0], Hz=fs)

for fname in glob.glob(os.path.join(args.filename, "*")):
    data = np.fromfile(fname, dtype=np.int16, offset=24)
    data = data.astype(float)
    data_iq = data[0::2] + 1.0j * data[1::2]


    ## plot time domain I/Q data
    #plt.plot(np.real(data_iq))
    #plt.plot(np.imag(data_iq))
    #plt.show()
    
    ## plot FFT of IQ data
    #plt.plot(10 * np.log10(np.abs(np.fft.fftshift(np.fft.fft(data_iq)))))
    #plt.show()

    ## downconvert from fs/4
    dc_array = np.array([ 1 + 0j,
                          0 - 1j, 
                         -1 + 0j,
                          0 + 1j])

    dc_array = np.tile(dc_array, len(data_iq)//4)
    dc_array = dc_array[:len(data_iq)]
    data_iq = data_iq * dc_array

    ## low pass filter (band select)
    #data_iq = np.convolve(data_iq, np.ones(4)/4 + 1j*(np.ones(4)/4))
    data_iq = np.convolve(data_iq, lp_taps)

    
    ## select time slice
    data_iq = data_iq[15000:25000]

    ## calculate phase
    t = 1.0 / fs * np.arange(len(data_iq)) * 1e3 - 1
    phase = np.unwrap(np.arctan2(np.imag(data_iq), np.real(data_iq)))
    phase = phase - np.linspace(phase[0], phase[-1], len(phase))
    freq = np.diff(phase) * fs / (2 * np.pi)

    ## plot phase vs time
    #plt.plot(t, phase)
    ## plot frequency vs time
    plt.plot(t[:-1], freq)
plt.xlabel("Time (ms)")
#plt.ylabel("Phase (rad)")
plt.ylabel("Frequency (Hz)")
plt.grid()
plt.show()

