#include <assert.h>
#include <libbladeRF.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <volk/volk.h>
#include <volk/volk_complex.h>
#include <unistd.h>
#include <pthread.h>
#include <poll.h>
#include <netdb.h> 
#include <netinet/in.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <arpa/inet.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>

#define SA struct sockaddr

// downconversion sampling scheme
//                        v DUT tone
//   |             |      ^        |
// -fs/2          f=0    fs/4     fs/2
//       /----------------------\      <= filter BW
//

// frontend setup
// 1. choose desired sample rate (= time resolution)
#define BLADERF_SPS  20000000
// 2. choose bandwidth < sample rate
#define BLADERF_BW   16000000
// 3. set frequency to (DUT frequency) - (fs/4)
//#define BLADERF_FREQ 1275000000
//#define BLADERF_FREQ 2571000000
//#define BLADERF_FREQ 315000000
#define BLADERF_FREQ 635000000

// trigger setup
// 1. define desired event length in samples - must be smaller than BUF_SIZE*RING_BUF_LEN-1 or so
// equation: event_time * sample rate, 220000 = 11 ms @ 20 Msps
#define EVENT_LEN_SPLS  120000
// define number of post-trigger samples (should be maybe 90% or so of total length)
// ex: 200000 = 10 ms @ Msps
#define EVENT_LEN_POST  100000
// choose transfer buffer size (number of samples) according to performance/USB needs (>4096, powers of two)
#define BUF_SIZE        131072
// choose number of sub buffers for ring buffer (total required memory = BUF_SIZE*RING_BUF_LEN*2*2 bytes)
// too many don't really hurt (memory is cheap)
#define RING_BUF_LEN    32
// printout of min/max per triggered block
//#define PRINT_MINMAX

#define PORT        8188
#define TCP_BUF_LEN 1024


// trigger detection logic, protected by trigger_mutex
pthread_mutex_t trigger_mutex = PTHREAD_MUTEX_INITIALIZER;
bool tcp_trigger_enable = false;
bool trigger_enabled = false;
int trig_id = 0;
int run_id = 0;
int pos_x, pos_y;

void *tcp_handler(void *args) {
    char buff[TCP_BUF_LEN]; 
    char tmp_buf[1024]; 
    int sockfd, len;
    struct pollfd pollsock[1];
    struct sockaddr_in servaddr;
    struct stat st = {0};

    int ret;

    bzero(&servaddr, sizeof(servaddr)); 
    servaddr.sin_family = AF_INET; 
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT); 

    while(true) {
        sockfd = socket(AF_INET, SOCK_STREAM, 0); 
        if (sockfd == -1) { 
            printf("socket creation failed...\n"); 
            exit(0); 
        } 

        if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) {
            printf("DAQ subscriber socket connection failed, retrying in 1 second...\n");
            sleep(1);
        } else {
            printf("DAQ subscriber socket connected.\n");
            bool conn_open = true;
            int read_count = 0;
            int read_err = 0;
            while(conn_open) {
                bzero(buff, TCP_BUF_LEN);
                read_count = read(sockfd, buff, sizeof(buff));
                read_err = errno;
                if (read_count <= 0) {
                    printf("Error while reading from Socket: %d\n", read_count);
                    printf("Errno: %s\n", strerror(read_err));
                    conn_open = false;
                    close(sockfd);
                } else {
                    if (strncmp(buff, "start_run ", 10) == 0) {
                        pthread_mutex_lock(&trigger_mutex);
                        trigger_enabled = true;
                        trig_id = 0;
                        pos_x = 999999;
                        pos_y = 999999;
                        sscanf(buff, "start_run %d", &run_id);

                        sprintf(tmp_buf, "data/run_%03d/", run_id);
                        if (stat(tmp_buf, &st) == -1) {
                            mkdir(tmp_buf, 0755);
                        }
                        sprintf(tmp_buf, "data/run_%03d/x_%06d/", run_id, pos_x);
                        if (stat(tmp_buf, &st) == -1) {
                            mkdir(tmp_buf, 0755);
                        }
                        sprintf(tmp_buf, "data/run_%03d/x_%06d/y_%06d/", run_id, pos_x, pos_y);
                        if (stat(tmp_buf, &st) == -1) {
                            mkdir(tmp_buf, 0755);
                        }
                        pthread_mutex_unlock(&trigger_mutex);
                        printf("Starting run %d.\n", run_id);
                    }
                    if (strncmp(buff, "stop_run", 8) == 0) {
                        pthread_mutex_lock(&trigger_mutex);
                        trigger_enabled = false;
                        pthread_mutex_unlock(&trigger_mutex);
                        printf("Stopping run %d.\n", run_id);
                    }
                    if (strncmp(buff, "pos ", 4) == 0) {
                        pthread_mutex_lock(&trigger_mutex);
                        sscanf(buff, "pos %d %d", &pos_x, &pos_y);
                        if (trigger_enabled) {
                            sprintf(tmp_buf, "data/run_%03d/x_%06d/", run_id, pos_x);
                            if (stat(tmp_buf, &st) == -1) {
                                mkdir(tmp_buf, 0755);
                            }
                            sprintf(tmp_buf, "data/run_%03d/x_%06d/y_%06d/", run_id, pos_x, pos_y);
                            if (stat(tmp_buf, &st) == -1) {
                                mkdir(tmp_buf, 0755);
                            }
                        }
                        pthread_mutex_unlock(&trigger_mutex);
                        printf("New position (%d|%d).\n", pos_x, pos_y);
                    }

                }
            }
            printf("Connection closed.\n");
        }
    }
}


int main(int argc, char *argv[])
{
    int status, i, ring_buf_count;
    size_t ring_buf_offs;
    unsigned int alignment = volk_get_alignment();
    char tmp_buf[1024];
    pthread_t tcp_thread;

    struct bladerf *dev = NULL;
    struct bladerf_devinfo dev_info;

    // initialize bladeRF hardware
    bladerf_init_devinfo(&dev_info);
    status = bladerf_open_with_devinfo(&dev, &dev_info);
    if (status != 0) {
        fprintf(stderr, "Unable to open device: %s\n",
                bladerf_strerror(status));
        return -1;
    }

    status = bladerf_set_frequency(dev, BLADERF_CHANNEL_RX(0), BLADERF_FREQ);
    if (status != 0) {
        fprintf(stderr, "Failed to set frequency: %s\n",
                bladerf_strerror(status));
        return -1;
    }

    status = bladerf_set_sample_rate(dev, BLADERF_CHANNEL_RX(0), BLADERF_SPS, NULL);
    if (status != 0) {
        fprintf(stderr, "Failed to set samplerate: %s\n",
                bladerf_strerror(status));
        return -1;
    }

    status = bladerf_set_bandwidth(dev, BLADERF_CHANNEL_RX(0), BLADERF_BW, NULL);
    if (status != 0) {
        fprintf(stderr, "Failed to set bandwidth: %s\n",
                bladerf_strerror(status));
        return -1;
    }
    
    status = bladerf_set_gain_mode(dev, BLADERF_CHANNEL_RX(0), BLADERF_GAIN_AUTOMATIC);
    if (status != 0) {
        fprintf(stderr, "Failed to set gain: %s\n", bladerf_strerror(status));
        return -1;
    }

    status = bladerf_sync_config(dev, BLADERF_RX_X1, BLADERF_FORMAT_SC16_Q11_META,
            16, BUF_SIZE, 8, 3500);
    if (status != 0) {
        fprintf(stderr, "Failed to configure RX sync interface: %s\n",
                bladerf_strerror(status));
        return -1;
    }

    // allocate RX ring buffer
    int16_t *ring_buf;
    ring_buf = (int16_t*) volk_malloc(sizeof(int16_t) * 2 * BUF_SIZE * RING_BUF_LEN, alignment);
    if (ring_buf == NULL) {
        perror("Failed to allocate RX ring buffer");
        return -1;
    }

    // create TCP thread
    pthread_create(&tcp_thread, NULL, tcp_handler, NULL);

    status = bladerf_enable_module(dev, BLADERF_RX, true);
    if (status != 0) {
        fprintf(stderr, "Failed to enable RX: %s\n", bladerf_strerror(status));
        return -1;
    }

    struct bladerf_metadata meta;
    struct timeval tv;
    bladerf_timestamp last_edge_ts = 0;
    bladerf_timestamp current_ts = 0;
    memset(&meta, 0, sizeof(meta));

    ring_buf_count = 0;
    bool triggered = false;
    int event_fd;
    printf("File format documentation:\n");
    printf("Hardware timestamp          - %d bytes.\n", sizeof(bladerf_timestamp));
    printf("System timestamp - seconds  - %d bytes.\n", sizeof(time_t));
    printf("System timestamp - useconds - %d bytes.\n", sizeof(suseconds_t));
    printf("Data (IQ sample files)      - %d bytes.\n", 2 * EVENT_LEN_SPLS * sizeof(int16_t));
    printf("\n");

    while(1) {
        ring_buf_offs = ring_buf_count * BUF_SIZE * 2;
        memset(&meta, 0, sizeof(meta));
        meta.flags = BLADERF_META_FLAG_RX_NOW;
        status = bladerf_sync_rx(dev, ring_buf + ring_buf_offs, BUF_SIZE, &meta, 5000);
        if (status != 0) {
            fprintf(stderr, "\n\nRX \"now\" failed: %s\n\n",
                    bladerf_strerror(status));
        } else {
            current_ts = meta.timestamp;
            if (meta.status & BLADERF_META_STATUS_OVERRUN) {
                fprintf(stderr, "\n\nOverrun detected. %u valid samples were read.\n", meta.actual_count);
            }

            //printf("RX'd %u samples at t=0x%016" PRIx64 "\n", meta.actual_count,
            //       meta.timestamp);

            // check whether to save some samples to file
            if (triggered && current_ts > (last_edge_ts + EVENT_LEN_POST)) {  // have enough post-trigger samples?
                triggered = false; 
#ifdef PRINT_MINMAX
                //// minmax
                int i;
                int16_t min = 0;
                int16_t max = 0;
                int16_t sample;
                for (i = 0; i < BUF_SIZE * 2; i++) {
                    sample = ring_buf[ring_buf_offs + i];
                    if (sample < min) {
                        min = sample;
                    }
                    if (sample > max) {
                        max = sample;
                    }
                }
                printf(" - Pk-Pk: %d / 4096 LSBs\n", max - min); 
#endif

                // calculate start and end indices of last event
                int ev_end_idx = ring_buf_offs - ((current_ts - (last_edge_ts + EVENT_LEN_POST)) * 2);
                assert(ev_end_idx % 2 == 0);
                int ev_start_idx = ev_end_idx - EVENT_LEN_SPLS * 2;
                assert(ev_start_idx % 2 == 0);
                assert(ev_end_idx > ev_start_idx);
                // bring event indices into ring buffer bounds
                ev_end_idx = (ev_end_idx < 0) ? ev_end_idx + (2 * BUF_SIZE * RING_BUF_LEN) : ev_end_idx;
                assert(ev_end_idx % 2 == 0);
                assert(ev_end_idx >= 0);
                assert(ev_end_idx < (2 * BUF_SIZE * RING_BUF_LEN));
                ev_start_idx = (ev_start_idx < 0) ? ev_start_idx + (2 * BUF_SIZE * RING_BUF_LEN) : ev_start_idx;
                assert(ev_start_idx % 2 == 0);
                assert(ev_start_idx >= 0);
                assert(ev_start_idx < (2 * BUF_SIZE * RING_BUF_LEN));

                if (trigger_enabled) {
                    pthread_mutex_lock(&trigger_mutex);
                    sprintf(tmp_buf, "data/run_%03d/x_%06d/y_%06d/trig_%06d.dat", run_id, pos_x, pos_y, trig_id);
                    trig_id++;
                    pthread_mutex_unlock(&trigger_mutex);

                    FILE* file = fopen(tmp_buf, "wb");
                    if (file != NULL) {
                        // write hardware timestamp to file
                        fwrite(&last_edge_ts, sizeof(bladerf_timestamp), 1, file);
                        // write system timestamp to file
                        gettimeofday(&tv, NULL);
                        fwrite(&tv.tv_sec, sizeof(time_t), 1, file);
                        fwrite(&tv.tv_usec, sizeof(suseconds_t), 1, file);
                        // write sample buffer to file
                        size_t num_written;
                        if (ev_end_idx > ev_start_idx) { // no wrap-around across ring buffer boundaries
                            num_written = fwrite(ring_buf + ev_start_idx, sizeof(int16_t), 2 * EVENT_LEN_SPLS, file);
                            assert(num_written == 2 * EVENT_LEN_SPLS);
                        } else {                         // wrap-around across ring buffer boundaries
                            // copy from start to end of buffer
                            num_written =  fwrite(ring_buf + ev_start_idx, sizeof(int16_t), ((2 * BUF_SIZE * RING_BUF_LEN) - ev_start_idx), file);
                            // copy from 0 to end of event
                            num_written += fwrite(ring_buf, sizeof(int16_t), ev_end_idx, file);
                            assert(num_written == 2 * EVENT_LEN_SPLS);
                        }
                        fclose(file);
                        printf("Event written to file %s\n", tmp_buf);
                    } else {
                        printf("File opening error...\n");
                    }
                }
            }
        }

        status = bladerf_get_timestamp(dev, BLADERF_RX, &meta.timestamp);
        if (status != 0) {
            fprintf(stderr, "Failed to get current RX timestamp: %s\n",
                    bladerf_strerror(status));
        } else {
            if (meta.timestamp > last_edge_ts) {
                printf("New event timestamp: 0x%016" PRIx64 "\n", meta.timestamp);
                last_edge_ts = meta.timestamp;
                triggered = true;
            }
        }
        
        // increment ring buffer pointer
        ring_buf_count++;
        if (ring_buf_count >= RING_BUF_LEN) {
            ring_buf_count = 0;
        }
    }

    bladerf_close(dev);
    return status;
}
